showq (0.4.1+git20200907-1) unstable; urgency=medium

  * New upstream version 0.4.1+git20200907
  * Add a very simple autopkgtest

 -- Dennis Braun <d_braun@kabelmail.de>  Wed, 09 Sep 2020 20:37:24 +0200

showq (0.4.1+git20200424-1) unstable; urgency=medium

  * New upstream snapshot 0.4.1+git20200424
  * Bump dh-compat to 13
  * d/copyright:
    + Update upstream email address
    + Update d/copyright year
    + Add myself to the d/ section
  * d/upstream/metadata: Add Archive and remove obsolete Name
  * Provide a bigger icon
  * Update desktop file

 -- Dennis Braun <d_braun@kabelmail.de>  Thu, 30 Apr 2020 23:52:14 +0200

showq (0.4.1+git20200218-1) unstable; urgency=medium

  * New upstream snapshot 0.4.1+git20200218
  * Drop 01-binary-spelling-fix.patch, applied by upstream
  * Add d/upstream/metadata
  * Set RRR: no
  * Add me as an uploader

 -- Dennis Braun <d_braun@kabelmail.de>  Tue, 31 Mar 2020 09:57:26 +0200

showq (0.4.1+git20180114-1) unstable; urgency=medium

  * Team upload

  [ Ondřej Nový ]
  * d/copyright: Use https protocol in Format field
  * d/control: Set Vcs-* to salsa.debian.org
  * Use debhelper-compat instead of debian/compat

  [ Felipe Sateler ]
  * Change maintainer address to debian-multimedia@lists.debian.org

  [ Olivier Humbert ]
  * Update showq.desktop (adds French menu item)
  * Update copyright (http -> https)

  [ Sebastian Ramacher ]
  * New upstream snapshot
    - Switch to meson (Closes: #938479)
  * debian/watch: Use uscan to fetch from git repo
  * debian/: Switch to meson
  * debian/docs: Remove docs that are note really useful
  * debian/control:
    - Bump Standards-Version
    - Bump debhelper compat to 12

 -- Sebastian Ramacher <sramacher@debian.org>  Sun, 16 Feb 2020 10:48:08 +0100

showq (0.4.1+git20161215~dfsg0-3) unstable; urgency=medium

  * Set dh/compat 11.
  * Hack prefix to get app started. (Closes: #883636)
    Thanks to James Cowgill <jcowgill@debian.org>
  * Bump Standards.
  * Use git instead of cgit in VCS.

 -- Jaromír Mikeš <mira.mikes@seznam.cz>  Tue, 02 Jan 2018 18:09:47 +0100

showq (0.4.1+git20161215~dfsg0-2) unstable; urgency=medium

  * Drop B-D libboost-dev.
  * Avoid useless linking.

 -- Jaromír Mikeš <mira.mikes@seznam.cz>  Tue, 02 Jan 2018 17:45:55 +0100

showq (0.4.1+git20161215~dfsg0-1) unstable; urgency=medium

  * Update upstream git location.
  * Get rid of waf blob on repack.
  * New upstream version 0.4.1+git20161215~dfsg0
  * Patch refreshed.
  * Bump Standards.
  * Fix VCS fields.
  * Sign tags.
  * Update homepage. (Closes: #821794)
  * Set dh/compat 10.
  * Drop dbg package in favour of dbgsym.
  * Fix some hardening.
  * Update copyright file.

 -- Jaromír Mikeš <mira.mikes@seznam.cz>  Thu, 15 Dec 2016 22:36:17 +0100

showq (0.4.1+git20151103~dfsg0-1) unstable; urgency=medium

  * Drop old repacking script.
  * Fix install docs.
  * Enable C++11 to prevent FTBFS.(closes: #805682)
  * Removed menu file (in response to the tech-ctte decision on #741573).

 -- Jaromír Mikeš <mira.mikes@seznam.cz>  Sat, 21 Nov 2015 18:19:05 +0100

showq (0.4.1+git20090622+dfsg0-2) unstable; urgency=medium

  * Bump Standards.
  * Set dh/compat 9.
  * Fix VCS fields.
  * Remove DMUA.
  * Add Keywords to desktop file.
  * Enable parallel build.
  * Pass LDFLAGS.

 -- Jaromír Mikeš <mira.mikes@seznam.cz>  Tue, 09 Jun 2015 23:17:29 +0200

showq (0.4.1+git20090622+dfsg0-1) unstable; urgency=low

  * Team upload.
  * Repack upstream tarball to get rid of the waf binary (Closes: #654502).
  * debian/rules: Properly prune *.pyc files.
  * debian/watch: Mangle debian's versioning.

 -- Alessio Treglia <alessio@debian.org>  Tue, 10 Jan 2012 14:41:29 +0100

showq (0.4.1+git20090622-1) unstable; urgency=low

  * Initial Release. (closes: #622151)

 -- Jaromír Mikeš <mira.mikes@seznam.cz>  Sat, 05 Feb 2011 21:38:37 +0100
